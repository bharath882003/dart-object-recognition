function [frames, descs] = getDescs_midpt(TD, param)

    rmin = param.rmin;
    rmax = param.rmax;
    nr = param.nr;
    nw = param.nw;
    minNumEvents = param.minNumEvents;
    
    r_bin_edges = logspace(log10(rmin),log10(rmax),nr);

%     R = 1:nr;
%     logDitance = [0 r_bin_edges];
%     midDistanceRho = (logDitance(R)+logDitance(R+1))/2;
%     midDistanceRho_for_interp =  repmat(midDistanceRho,nw,1);  
%     
%     rowIdx=1:nw;
%     stepang=2*pi/nw;
%     AngleTheta = (stepang*rowIdx+stepang*(rowIdx-1))/2;
%     Angletheta_for_interp = repmat(AngleTheta, nr,1)';
% 
%     [gen_x,gen_y] = pol2cart(Angletheta_for_interp(:),midDistanceRho_for_interp(:));
%     originx = rmax + 1;
%     originy = rmax + 1;
%     interpx = double(originx + gen_x);
%     interpy = double(originy - gen_y);
%     
    %define the grid of pixels
    filename = ['template_dart_midpts num2str(rmax) .mat']; 
  if exist(filename, 'file') ~= 2
    originx = rmax + 1;
    originy = rmax + 1;
    [X,Y] = meshgrid(1:2*rmax+1, 1:2*rmax+1);
    xArr = X(:);
    yArr = Y(:);
    rad = sqrt( (xArr - originx).^2 + (yArr - originy).^2 );
    theta = atan2(yArr - originy, xArr - originx);
    
    theta = reshape(theta, size(X));
    wedgeNum = ceil(nw* theta ./ (2*pi)) + nw/2;
    
    ringNum = bsxfun(@lt, r_bin_edges(1:end),rad);
    ringNum = sum(ringNum, 2) + 1;
    ringNum = reshape(ringNum, size(X));
    
    outside_rmax = ringNum > nr;
    
    ringNum(outside_rmax) = NaN;
    wedgeNum(outside_rmax) = NaN;
    
    for i = 1:nr
        for j = 1:nw
           logic_binimage = ringNum == i & wedgeNum == j;
           if sum(logic_binimage(:)) > 0
               [y,x] =find(logic_binimage);
               interpx_mat(j,i) = mean(x);
               interpy_mat(j,i) = mean(y);
           else
               interpx_mat(j,i) = NaN;
               interpy_mat(j,i) = NaN;
           end
        end
    end
    
    interpx = interpx_mat(:);
    interpy = interpy_mat(:); 
    
    for i = 1:numel(X)
      if ~isnan(ringNum(i))  
       [~, closest_bins]= pdist2([interpx interpy],[X(i) Y(i)],'Euclidean','Smallest',4); 
        closest_xys = [interpx(closest_bins) interpy(closest_bins)];
        
        if ~any(isnan(closest_xys(:)))
            temp_m = [ 1 closest_xys(1,1) closest_xys(1,2) closest_xys(1,1)*closest_xys(1,2); ...
                       1 closest_xys(2,1) closest_xys(2,2) closest_xys(2,1)*closest_xys(2,2); ...
                       1 closest_xys(3,1) closest_xys(3,2) closest_xys(3,1)*closest_xys(3,2); ...
                       1 closest_xys(4,1) closest_xys(4,2) closest_xys(4,1)*closest_xys(4,2)];

            probabilities = inv(temp_m)' * [1;X(i);Y(i);X(i)*Y(i)];

            if all(probabilities >= 0) && sum(probabilities) == 1                    
               closest_neighbours_probs{i} = [closest_bins' probabilities'];  
            else
%                 log_holder = probabilities > 0;
%                 probabilities(~log_holder) = 0;
%                 probabilities = probabilities./(sum(probabilities));
%                 closest_bins(~log_holder) = 0;
                closest_neighbours_probs{i} = [closest_bins(1) zeros(1,3) 1 zeros(1,3)]; 
            end
        end
      end
    end
    save(filename,'closest_neighbours_probs');
  else
      load(filename);
  end

    %writeFile('TD.txt', TD);
    descs = uint16(zeros(nr*nw, 1));
    frames = uint8(zeros(2, 1));
    
    first_saccade_events = TD.ts <= 105e3;

    first_saccade_pt = 1;
    last_sampling_pt =find(first_saccade_events == 1, 1, 'last');
    last_saccade_pt = last_sampling_pt - minNumEvents;
    tic
    parfor i=first_saccade_pt:last_saccade_pt
        [frame, desc] = getDescriptors(TD, closest_neighbours_probs, r_bin_edges, nr, nw, rmax, first_saccade_pt, i + minNumEvents);
        frames(:,i) = uint8(frame);     
        descs(:,i) = uint16(desc);
    end
    
    second_saccade_events = TD.ts > 105e3 & TD.ts <= 210e3;
    second_saccade_pt = find(second_saccade_events  == 1, 1, 'first') ;
    last_sampling_pt = find(second_saccade_events == 1, 1, 'last');
    last_saccade_pt = last_sampling_pt - 2*minNumEvents;
    parfor i=second_saccade_pt-minNumEvents:last_saccade_pt
        [frame, desc] = getDescriptors(TD, closest_neighbours_probs, r_bin_edges, nr, nw, rmax, second_saccade_pt, i+2*minNumEvents);
        frames(:,i) = uint8(frame);     
        descs(:,i) = uint16(desc);
    end
    
    third_saccade_events = TD.ts > 210e3;
    third_saccade_pt = find(third_saccade_events  == 1, 1, 'first') ;
    last_sampling_pt = find(third_saccade_events == 1, 1, 'last');
    last_saccade_pt = last_sampling_pt - 3*minNumEvents;
    parfor i=third_saccade_pt-2*minNumEvents:last_saccade_pt
        [frame, desc] = getDescriptors(TD, closest_neighbours_probs, r_bin_edges, nr, nw, rmax, third_saccade_pt, i+3*minNumEvents);
        frames(:,i) = uint8(frame);     
        descs(:,i) = uint16(desc);
    end
    toc
end

function [frame, desc] = getDescriptors(TD, closest_neighbours_probs, r_bin_edges, nr, nw, rmax, first_saccade_pt, i)
    logHistEmpty = zeros(nw, nr);

    currSpikeArr.x = TD.x(first_saccade_pt:i);
    currSpikeArr.y = TD.y(first_saccade_pt:i);
    
    [currSpikeArr.rad, currSpikeArr.theta, frame] = getLPCoord(currSpikeArr.x, currSpikeArr.y);
    currSpikeArr.wedgeNum = ceil(nw* currSpikeArr.theta / (2*pi)) + nw/2;
    ringNum = bsxfun(@lt, r_bin_edges(1:end),currSpikeArr.rad);
    currSpikeArr.ringNum = sum(ringNum, 2) + 1;
    discardedIndices = find(currSpikeArr.ringNum > nr);
    currSpikeArr.ringNum(discardedIndices) = [];
    currSpikeArr.wedgeNum(discardedIndices) = [];
    currSpikeArr.x(discardedIndices) = [];
    currSpikeArr.y(discardedIndices) = [];
    
    % Get relative location  to template
    all_x = currSpikeArr.x - frame(1) + rmax + 1;
    all_y = currSpikeArr.y - frame(2) + rmax + 1;
    
    all_ind = sub2ind([2*rmax+1 2*rmax+1], all_y, all_x); 

    slices = {closest_neighbours_probs{all_ind}};
    slices = slices';
    slices = cell2mat(slices);
    
    bin_locations = slices(:,1:4);
    prob_locations = slices(:,5:end);
    log_err = prob_locations == 0;
    bin_locations(log_err) = [];
    prob_locations(log_err) = [];
    
    %assuming good split
    slices = [bin_locations' prob_locations'];
    
    % first colums is the wedge and ring number
    [wedgeNum, ringNum] = ind2sub([nw nr], slices(:,1));
    
    logHist = accumarray([wedgeNum(:), ringNum(:)], slices(:,2));
    %logHist = logHist/sum(logHist(:));
    logHistEmpty(1:size(logHist,1), 1:size(logHist, 2)) = logHistEmpty(1:size(logHist,1), 1:size(logHist, 2)) + logHist;
    desc = logHistEmpty(:);
end

function [rad, theta, frame] =  getLPCoord(xArr, yArr)
    xc = xArr(end);
    yc = yArr(end);
    rad = sqrt( (xArr - xc).^2 + (yArr - yc).^2 );
    theta = atan2(yArr - yc, xArr - xc);
    frame = [xc yc]';
end

function writeFile(fileName, var)
    writetable(struct2table(var), fileName);
end
