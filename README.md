# DART Object Recognition

The VLFeat Library is provided with the repo and the MATLAB script configures it on-the-fly.

Also the MATLAB AER Toolbox from www.garrickorchard.com is provided with the repo. 

Source code script:
Event_context_NCaltech101_allRB_rmax10CROPmidpt.m

# Object tracking annotations

The folder 'SHAPES_GROUND_TRUTH' contains three MAT files, each for the case of translation, rotation and 6-DOF Shapes data. 

In turn, each MAT file contains ground annotations for the seven shapes, namely Heart, Hexagon, LShape, Star, Rectangle, Triangle and Oval.

The ground truth table has 5 columns: timestamp, top_left_x, top_left_y, width and height of the bounding boxes.



