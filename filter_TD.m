function filtered_TD = filter_TD(TD, us_time_filter, us_time_filter_alt)
    new_TD = FilterTD(TD, us_time_filter) ;
    
    numEvents = numel(TD.x);
    numEventsFiltered = numel(new_TD.x);
    
    if (numEventsFiltered/numEvents < 0.75)
        filtered_TD = FilterTD(TD, us_time_filter_alt) ;
    else
        filtered_TD = new_TD;
    end
end